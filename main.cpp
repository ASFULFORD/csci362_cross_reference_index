#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "bs_tree.h"
#define FILE "text2.txt"
using namespace std;
int main()
{
	/**
	* Walker node used to iterate the number list
	*/
	struct line_list *walker = new line_list;
	walker = NULL;
	/**
	* Temp node used to store temporary node for manipulation
	*/
	struct node *temp = new node;
	temp = NULL;
	/**
	 * Instantiation of bs_tree class
	*/
	bs_tree bs_tree;
	/**
	* Variables
	*/
	char ch;
	string key, line;
	int word_length = 0, line_num = 1, line_index = 0;
	/**
	* Read in file
	*/
	ifstream myfile(FILE);
	
	if(myfile.is_open())/*Test if the file can be opened*/
	{
		while(getline(myfile, line))/*While there is a line of text available, this loop will run*/
		{
			if(line == "")
				continue;
			while(line_index < line.length() - 1)/*While there is a character available in the line, this loop will run*/	
			{
				ch = line[line_index];
				if(ch == '#')/*test for end of file (#)*/
					break;
				if(ch == '.' || ch == ',' || ch == ' ' || ch == '\n' || ch == '\t')/*Test characters for delimiters*/
				{
					if(word_length > 10)/*Test if the word length is longer than 10*/
					{
						key.erase(10, key.size() - 10);/*If so, delete extra letters*/
					}
					if(word_length > 0)/*Test to see if there is a word ready for insertion*/
					{
						temp = bs_tree.search(key);/*Create a temporary node that matches key*/
						if(temp != NULL)/*If a match is found...*/
						{
							walker = temp->line_list;/*set line walker to head of number list*/
							
							while(walker->next != NULL)/*move walker to the end of the list*/
								walker = walker->next;

							walker->next = new line_list;
							walker->next->line_num = line_num;/*insert line_num into the word's line list*/
							walker->next->next = NULL;
							/*Basically, this portion adjusts only the line attribute of the node*/
						}
						else
						{
							/*If no match is found, insert node*/
							bs_tree.insert(key, line_num);
						}
					}
					key.clear();/*reset the key string*/
					word_length = 0;/*reset the length of the word*/
					line_index++;/*increment the index of the line so we can look at the next character*/

					if(line_index >= line.size())/*If the number of characters is >= the size of the line, we know we are at the end.*/
						break;/*break out of loop*/

					continue;/*continue to look at characters in the line if we're not at the end*/
				}
				if(ch >= '0' && ch <= '9')/*test to see if the character is a number*/
				{
					if(word_length == 0)/*test to see if the number is the first letter*/			
					{
						line_index++;/*if so, look at next letter and ignore*/
						continue;
					}
					else/*if not, add the character to the key string, increment word length and look at next character*/
					{
						key.insert(key.end(), ch);
						word_length++;
						line_index++;
					}
				}
				if((ch >= 'a' && ch <= 'z') || ( ch >= 'A' && ch <= 'Z'))/*test to see if the character is a letter*/
				{
					key.insert(key.end(), ch);/*if so, add character to the key string, increment word length and look at next character*/
					word_length++;
					line_index++;
				}
			}
			/*once the entire line has been looked at, reset the line index back to 0 and increment the line number*/
			line_index = 0;
			line_num++;
		}
		/*once the entire file has been looked at, close the file, and print the tree in an inorder traversal*/
		myfile.close();
		bs_tree.print_tree();
	}
	else
	{
		cout << "Unable to open file.\n";
	}
	return(1);
}
