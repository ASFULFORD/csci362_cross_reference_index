main: main.o bs_tree.o
	g++ -g -o main main.o bs_tree.o
main.o: main.cpp
	g++ -c -g -include bs_tree.h main.cpp
bs_tree.o: bs_tree.cpp
	g++ -c -g -include bs_tree.h bs_tree.cpp
clean:
	rm -f *.o main
