#include "bs_tree.h"
#include <iostream>
#include <iomanip>
using namespace std;
/**
 * Constructor
 */
bs_tree::bs_tree()
{
	root = NULL;
}
/**
 * Deconstructor
 */
bs_tree::~bs_tree()
{
	destroy_tree();
}
/**
 * Internal version of destroy_tree()
 */
void bs_tree::destroy_tree(node *leaf)
{
	if(leaf != NULL)
	{
		destroy_tree(leaf->left);
		destroy_tree(leaf->right);
		delete leaf;
	}
}
/**
 * Internal version of insert()
 */
void bs_tree::insert(string key, int line, node *leaf)
{
	struct line_list *walker = new line_list;

	if(key < leaf->key)
	{
		if(leaf->left != NULL)
			insert(key, line, leaf->left);
		else
		{
			leaf->left = new node;
			leaf->left->line_list = new line_list;
			leaf->left->key = key;
			walker = leaf->left->line_list;
			walker->line_num = line;
			walker->next = NULL;
			leaf->left->left = NULL;
			leaf->left->right = NULL;
		}  
	}
	else if(key >= leaf->key)
	{
		if(leaf->right != NULL)
			insert(key, line, leaf->right);
		else
		{
			leaf->right = new node;
			leaf->right->line_list = new line_list;
			leaf->right->key = key;
			walker = leaf->right->line_list;
			walker->line_num = line;
			walker->next = NULL;
			leaf->right->left = NULL;
			leaf->right->right = NULL;
		}
	}
}
/**
 * internal version of search()
 */
node *bs_tree::search(string key, node *leaf)
{
	if(leaf != NULL)
	{
		if(key == leaf->key)
			return leaf;
		if(key < leaf->key)
			return search(key, leaf->left);
		else
			return search(key, leaf->right);
	}
	else return NULL;
}
/**
 * internal version of print_tree()
 */
void bs_tree::print_tree(node *leaf)
{
	if(leaf != NULL)
	{
		struct line_list *walker = new line_list;
		walker = leaf->line_list;
		print_tree(leaf->left);
		cout << setw(15) << left << leaf->key;
		if(walker->next == NULL)
			cout << setw(8) << left << walker->line_num;
		while(walker->next != NULL)
		{
			cout << setw(8) << left << walker->line_num;
			walker = walker->next;
		}
		cout << '\n';
		print_tree(leaf->right);
	}
}
/**
 * internal version of print_tree()
 */
/**
 * external version of insert()
 */
void bs_tree::insert(string key, int line)
{
	struct line_list *walker = new line_list;
	if(root != NULL)
		insert(key, line, root);
	else
	{
		root = new node;
		root->line_list = new line_list;
		walker = root->line_list;
		root->key = key;
		walker->line_num = line;
		walker->next = NULL;
		root->left = NULL;
		root->right = NULL;
	}
}
/**
 * external version of search()
 */
node *bs_tree::search(string key)
{
	return search(key, root);
}
/**
 * external version of destroy_tree()
 */
void bs_tree::destroy_tree()
{
	destroy_tree(root);
}
/**
 * external version of print_tree()
 */
void bs_tree::print_tree()
{
	print_tree(root);
}