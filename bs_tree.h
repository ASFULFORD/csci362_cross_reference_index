#ifndef BS_TREE_H_EXISTS
#define BS_TREE_H_EXISTS
#include <string>
using namespace std;
/**
 * line node for number list
 */
struct line_list
{
	int line_num;
	struct line_list *next;
};
/**
 * binary node for binary search tree
 */
struct node
{
	string key;
	string line;
	struct line_list *line_list;
	struct node *left;
	struct node *right;
};
/**
 * binary search tree class
 */
class bs_tree
{
	private:
		node *root;
		void destroy_tree(node *leaf);
		void insert(string key, int line, node *leaf);
		node *search(string key, node *leaf);	
		void print_tree(node *leaf);
	public:
		bs_tree();
		~bs_tree();
		void insert(string key, int line);
		node *search(string key);
		void destroy_tree();
		void print_tree();
};
#endif